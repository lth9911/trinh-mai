//
//  CustomCollectionViewCell.swift
//  Trinh Mai
//
//  Created by Hieu Le on 6/4/15.
//  Copyright (c) 2015 Hieu Le. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    var textView:UITextView!
    var imageView:UIImageView!
    
    required init(coder aDecoder:NSCoder){
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        imageView = UIImageView(frame: CGRectMake(0, 0, frame.size.width, frame.size.height))
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        
        contentView.addSubview(imageView)
        
        let tvFrame = CGRectMake(0, frame.size.height * 4/5, frame.size.width, frame.size.height)
        textView = UITextView(frame: tvFrame)
        textView.font = UIFont.systemFontOfSize(22.0)
        textView.backgroundColor = UIColor(white: 1.0, alpha: 0.3)
        textView.textAlignment = .Center
        textView.userInteractionEnabled = false
        textView.editable = false
        
        contentView.addSubview(textView)
        
    }
}
