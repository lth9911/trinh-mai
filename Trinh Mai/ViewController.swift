//
//  ViewController.swift
//  Trinh Mai
//
//  Created by Hieu Le on 6/3/15.
//  Copyright (c) 2015 Hieu Le. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    var collection: UICollectionView?
    
    var catItems = [String]()
    var catItemsDict = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set navigation controller + tab bar controller color
        //self.navigationController?.navigationBar.barTintColor = .blackColor()
//        self.navigationController?.navigationBar.tintColor = .grayColor()
//        
//        //self.tabBarController?.tabBar.barTintColor = .blackColor()
//        self.tabBarController?.tabBar.tintColor = .grayColor()
//        
        let label = UILabel()
        label.text = "Trịnh Mai"
        label.textColor = .blackColor()
        label.textAlignment = .Center
        label.sizeToFit()
        label.font = UIFont.systemFontOfSize(22.0)
        self.navigationItem.titleView = label
        
        
        //initiate
        self.catItems = LocationInfo().getCatImages()
        self.catItemsDict = LocationInfo().getCatDict()
        
        let imageString = catItems[0]
        let image = UIImage(named: imageString)
        let imageView = UIImageView(image: image)
        imageView.frame = CGRectMake(0, 0, self.view.frame.width, 250)
        imageView.contentMode = UIViewContentMode.ScaleToFill
        imageView.clipsToBounds = true
        
        self.view.addSubview(imageView)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        // navbar height + tab bar heihgt
        let navH = self.navigationController!.navigationBar.frame.height + 10
        let tabH = self.tabBarController!.tabBar.frame.height
        
        self.collection = UICollectionView(frame: CGRectMake(0, navH, self.view.frame.width, self.view.frame.height - navH - tabH), collectionViewLayout: layout)
        self.collection!.delegate = self
        self.collection!.dataSource = self
        self.collection!.registerClass(CustomCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(self.collection!)
        
        // Do any additional setup after loading the view.
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CustomCollectionViewCell
        
        cell.imageView.image = UIImage(named: catItemsDict[indexPath.row]["imageStringName"] as! String)
        cell.textView.text = catItemsDict[indexPath.row]["title"] as? String
//        cell.backgroundColor = UIColor.redColor()
//        
//        let newCellView = UIView(frame: cell.frame)
//        newCellView.backgroundColor = .grayColor()
//        
//        let imgV = UIImageView(frame: cell.frame)
//        imgV.image = UIImage(named: catItemsDict[indexPath.row]["imageStringName"] as! String)
//        imgV.contentMode = UIViewContentMode.ScaleAspectFill
//        imgV.clipsToBounds = true
//        
////        cell.backgroundView = imgV
//        
//        let label = UILabel(frame: cell.frame)
//        label.text = catItemsDict[indexPath.row]["title"] as? String
//        
//        newCellView.addSubview(imgV)
//        newCellView.addSubview(label)
//        
//        cell.backgroundView = newCellView
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catItems.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 2.5
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height/3.0-5)
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        println(indexPath.row)
        let vc = CatItemViewController()
        vc.itemTitle = catItemsDict[indexPath.row]["title"] as! String
        vc.imageString = catItemsDict[indexPath.row]["imageStringName"] as! String
        vc.itemDescription = catItemsDict[indexPath.row]["description"] as! String
        
        self.navigationController?.showViewController(vc, sender: self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
