//
//  ContactUsViewController.swift
//  Trinh Mai
//
//  Created by Hieu Le on 6/3/15.
//  Copyright (c) 2015 Hieu Le. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController, UIWebViewDelegate {
    
    override func viewWillAppear(animated: Bool) {
        
        let height = self.view.frame.height-50
        
        let embedForm = "<div id=\"wufoo-ru5wsat1pazv2h\"></div><div id=\"wuf-adv\" style=\"font-family:inherit;font-size: small;color:#a7a7a7;text-align:center;display:block;\"></div><script type=\"text/javascript\">var ru5wsat1pazv2h;(function(d, t) {var s = d.createElement(t), options ={'userName':'lth9911','formHash':'ru5wsat1pazv2h','autoResize':true,'height':'\(height)','async':true,'host':'wufoo.com','header':'show','ssl':true};s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'www.wufoo.com/scripts/embed/form.js';s.onload = s.onreadystatechange = function() {var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;try { ru5wsat1pazv2h = new WufooForm();ru5wsat1pazv2h.initialize(options);ru5wsat1pazv2h.display(); } catch (e) {}};var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);})(document, 'script');</script>"
        super.didReceiveMemoryWarning()
        let embedCode = "<html><head></head><body>\(embedForm)</body></html>"
        let webView = UIWebView(frame: CGRectMake(0, 40, self.view.frame.width, height))
        webView.loadHTMLString(embedCode, baseURL: nil)
        //        webView.backgroundColor = .blackColor()
        //        webView.scrollView.scrollEnabled = false
        webView.scrollView.bounces = false
        webView.delegate = self
        
        self.view.addSubview(webView)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}


