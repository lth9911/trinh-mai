//
//  LocationInfo.swift
//  Trinh Mai
//
//  Created by Hieu Le on 6/3/15.
//  Copyright (c) 2015 Hieu Le. All rights reserved.
//

import UIKit

class LocationInfo: NSObject {
    
    // Contact Info
    var hours = "10:00a.m to 10:00p.m"
    var phone = "100-000-0000"
    var website = "https://www.facebook.com/trinhmaistore"
    
    var addresses: Array<[String:AnyObject]> = [
        [
            "title":"My 1st Location",
            "address":"1006 Caddington Ave, Silver Spring, MD 20901",
            "defaultPoint": true,
            "lat": 39.037536,
            "long": -77.011274,
        ],
        [
            "title":"My 2nd Location",
            "address":"Ngõ 61 Giang Văn Minh, Kim Mã, Hà Nội, Vietnam",
            "defaultPoint": false,
            "lat": 21.0330971,
            "long": 105.8273184,
        ]
    ]
    
    //initial view
    var initialImageStrings = [123, "ladies", "kids", "men","appliances"]
    var finalImageArray = [String]()
    var catItems: Array<[String:AnyObject]> = [
        [
            "title":"Ladies",
            "description":"For the Ladies",
            "imageStringName": "ladies",
            "id": 1,
            
        ],
        [
            "title":"Kids",
            "description":"For the Kids",
            "imageStringName": "kids",
            "id": 2,
            
        ],
        [
            "title":"Men",
            "description":"For the Men",
            "imageStringName": "men",
            "id": 3,
            
        ],
        [
            "title":"Home",
            "description":"Household Appliances",
            "imageStringName": "appliances",
            "id": 4,
            
        ],

    ]
    
    required override init(){
        super.init()
        for item in catItems {
            let itemAdd = item["imageStringName"] as? String
            if itemAdd != nil {
                self.addImageToArray(itemAdd!)
            }
        }
    }
    func getCatDict() -> Array<[String:AnyObject]> {
        return self.catItems
    }
    
    func getWebsiteURL() -> NSURL {
        let url: NSURL = NSURL(string: self.website)!
        return url
    }
    
    func getPhone() -> String {
        return self.phone
    }
    
    func getGeneralHours() -> String {
        return self.hours
    }
    
    func getCatImages() -> Array<String> {
        return self.finalImageArray
    }
    
    func getDefaultAddresses() -> Array<[String:AnyObject]> {
        return self.addresses
    }
    func addImageToArray(imageString: String){
        let imageTest = UIImage(named: imageString)
        if imageTest != nil {
            self.finalImageArray.append(imageString)
        }
    }
}
